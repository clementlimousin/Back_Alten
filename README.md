# API Flask pour la gestion des produits

Cette API Flask permet de gérer des produits, y compris la création, la récupération, la mise à jour et la suppression de produits dans une base de données SQLite.

## Installation

Avant de commencer, assurez-vous d'avoir Python et pip installés sur votre système. Ensuite, vous pouvez installer les dépendances du projet en exécutant la commande suivante :

```
pip install -r requirements.txt
```

## Configuration de la base de données

Cette application utilise une base de données SQLite. Vous pouvez créer la base de données.

## Utilisation de l'API

L'API expose les endpoints suivants :

- `GET /products` : Récupère tous les produits.
- `GET /products/<product_id>` : Récupère les détails d'un produit spécifique.
- `POST /products` : Crée un nouveau produit.
- `PATCH /products/<product_id>` : Met à jour un produit existant.
- `DELETE /products/<product_id>` : Supprime un produit existant.

Les requêtes POST et PATCH nécessitent un corps JSON contenant les détails du produit. Voici un exemple de corps JSON pour la création d'un produit :

```json
{
    "code": "ABC123",
    "name": "Produit ABC",
    "description": "Description du produit ABC",
    "price": 19.99,
    "quantity": 100,
    "inventoryStatus": "En stock",
    "category": "Catégorie ABC",
    "image": "lien_vers_image",
    "rating": 4.5
}
```

## Exécution de l'application
Pour exécuter l'application, exécutez le fichier app.py :

```
python app.py
```


L'application sera exécutée sur http://localhost:5000.