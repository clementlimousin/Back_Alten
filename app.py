from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///products.db'  # Chemin vers la base de données SQLite
db = SQLAlchemy(app)
ma = Marshmallow(app)


# Modèle de produit
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(50), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    price = db.Column(db.Float, nullable=True)
    quantity = db.Column(db.Integer, nullable=True)
    inventoryStatus = db.Column(db.String(20), nullable=True)
    category = db.Column(db.String(50), nullable=True)
    image = db.Column(db.String(100), nullable=True)
    rating = db.Column(db.Float, nullable=True)

class ProductSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Product


# Gestionnaire de contexte pour la création de tables
with app.app_context():
    db.create_all()
    
# GET /products
@app.route('/products', methods=['GET'])
def get_products():
    try:
        products = Product.query.all()
        output = ProductSchema(many=True).dump(products)
        return jsonify(output)
    except Exception as e:
         return jsonify({"error": str(e)}), 500


# POST /products
@app.route('/products', methods=['POST'])
def create_product():
    try:
        # Vérifier si les champs obligatoires sont présents dans la demande
        required_fields = ['code', 'name']
        for field in required_fields:
            if field not in request.json:
                return jsonify({"error": f"Le champ '{field}' est obligatoire"}), 400
        # Créer un nouveau produit 
        new_product_data = request.json
        new_product = Product(**new_product_data)
        db.session.add(new_product)
        db.session.commit()
        output = ProductSchema().dump(new_product)
        return jsonify(output), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": str(e)}), 500

# GET /products/<int:product_id>
@app.route('/products/<int:product_id>', methods=['GET'])
def get_product(product_id):
    try:
        product = Product.query.get(product_id)
        if product:
            output = ProductSchema().dump(product)
            return jsonify(output)
        else:
            return jsonify({"error": "Product not found"}), 404
    except Exception as e:
        return jsonify({"error": "An error occurred while retrieving the product"}), 500

# PATCH /products/<int:product_id>
@app.route('/products/<int:product_id>', methods=['PATCH'])
def update_product(product_id):
    try:
        # Vérifier si les champs obligatoires sont présents dans la demande
        required_fields = ['code', 'name']
        for field in required_fields:
            if field not in request.json:
                return jsonify({"error": f"Le champ '{field}' est obligatoire"}), 400
        # Update le produit
        product = Product.query.get(product_id)
        if product:
            data = request.json
            for key, value in data.items():
                setattr(product, key, value)
            db.session.commit()
            output = ProductSchema().dump(product)
            return jsonify(output)
        else:
            return jsonify({"error": "Product not found"}), 404
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": "An error occurred while updating the product"}), 500

# DELETE /products/<int:product_id>
@app.route('/products/<int:product_id>', methods=['DELETE'])
def delete_product(product_id):
    try:
        product = Product.query.get(product_id)
        if product:
            # Delete le produit
            db.session.delete(product)
            db.session.commit()
            return jsonify({"message": "Product deleted"}), 200
        else:
            return jsonify({"error": "Product not found"}), 404
    except Exception as e:
        db.session.rollback()
        return jsonify({"error": "An error occurred while deleting the product"}), 500

if __name__ == '__main__':
    app.run(debug=True)